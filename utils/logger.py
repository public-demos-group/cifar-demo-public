import logging


def get_logger(name: str):
    """Logs a message.

    Args:
        name(str): name of logger
    """
    logger = logging.getLogger(name)
    return logger

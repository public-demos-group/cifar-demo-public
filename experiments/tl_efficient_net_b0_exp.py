from experiments.exp_configs.exp_config import ExperimentConfig
from models.efficientnet_model import EfficientNetModelCifar10
from models.model_configs.model_config import ModelConfig


def run():
    input_shape = (32, 32, 3)
    output_dim = 10

    model_cfg = ModelConfig(input_shape, output_dim)
    exp_cfg = ExperimentConfig(model_cfg)
    efn_x = EfficientNetModelCifar10(exp_cfg)

    efn_x.build()
    efn_x.train()
    efn_x.evaluate()


if __name__ == '__main__':
    run()

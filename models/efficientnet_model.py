from PIL.Image import Image
from keras import Sequential
from keras.datasets import cifar10
from keras.utils import to_categorical
from tensorflow.keras import applications as k_applications
from tensorflow.keras import callbacks as k_callbacks
from tensorflow.keras import layers as k_layers
from tensorflow.keras import models as k_models

from experiments.exp_configs.exp_config import ExperimentConfig
from models.base_model import BaseModel
from utils.logger import get_logger

my_logger = get_logger('my_logger')


class EfficientNetModelCifar10(BaseModel):
    def __init__(self, exp_cfg: ExperimentConfig):
        super().__init__(exp_cfg.model_config)
        self.exp_cfg = exp_cfg
        self.model = None
        self.input_shape = exp_cfg.model_config.input_shape
        self.output_dim = exp_cfg.model_config.output_dim
        base_model = k_applications.EfficientNetB0(
            include_top=False, weights='imagenet', drop_connect_rate=0.4
        )
        # base_model.trainable = False
        self.backbone = base_model
        self.dataset = self._load_data()

    @staticmethod
    def _load_data() -> tuple:
        """Loads datasets and scales samples.
        Returns:
            Train, test tuple
        """
        my_logger.info("Loading CIFAR-10 dataset...")
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        my_logger.info("\t... CIFAR-10 downloaded.")

        my_logger.info(f"x_train shape: {x_train.shape}")
        my_logger.info(f"x_test shape: {x_test.shape}")

        my_logger.info(f"Normalizing dataset...")
        # Cast int to float to be able to perform division by 255 (
        # normalization)
        # Divide with 255.0 to normalize to [0,1] range
        x_train = x_train.astype('float32') / 255.0
        x_test = x_test.astype('float32') / 255.0

        # Convert class vectors to  encoding
        y_train = to_categorical(y_train, 10)
        y_test = to_categorical(y_test, 10)
        my_logger.info(f"... dataset normalized.")

        return (x_train, y_train), (x_test, y_test)

    @staticmethod
    def _augment_img() -> Sequential:
        img_augmentation = Sequential(
            [
                k_layers.RandomRotation(factor=0.15),
                k_layers.RandomTranslation(height_factor=0.1, width_factor=0.1),
                k_layers.RandomFlip(),
                k_layers.RandomContrast(factor=0.1),
            ],
            name="img_augmentation",
        )
        return img_augmentation

    def build(self):
        """Builds EfficientNet based architecture with specified head."""
        inputs = k_layers.Input(shape=self.input_shape)

        x = self._augment_img()(inputs)
        x = self.backbone(x, training=False)
        x = k_layers.Conv2D(
            filters=32,
            kernel_size=(3, 3),
            padding='same',
            activation='relu',
        )(x)
        x = k_layers.GlobalAveragePooling2D()(x)
        x = k_layers.Dropout(0.2)(x)  # Regularize with dropout
        output = k_layers.Dense(self.output_dim)(x)

        self.model = k_models.Model(inputs=inputs, outputs=output)

    def train(self):
        """Compile and train model."""
        my_logger.info("Model training started...")
        self.model.compile(
            optimizer=self.exp_cfg.optimizer,
            loss=self.exp_cfg.loss,
            metrics=self.exp_cfg.metrics,
        )
        tensorboard_logger = k_callbacks.TensorBoard(log_dir='./')

        model_history = self.model.fit(
            x=self.dataset[0][0],
            y=self.dataset[0][1],
            batch_size=self.exp_cfg.batch_size,
            epochs=self.exp_cfg.number_epochs,
            steps_per_epoch=self.dataset[0][0].shape[0] // self.exp_cfg.batch_size,
            validation_data=(self.dataset[1][0], self.dataset[1][1]),
            shuffle=True,
            callbacks=[tensorboard_logger],
        )

        my_logger.info("... model training finished.")

        return model_history

    def evaluate(self):
        loss, acc = self.model.evaluate(
            x=self.dataset[1][0], y=self.dataset[1][1]
        )

        LOG.info(f"Test loss: {loss} / Test accuracy: {acc}")
        return loss, acc

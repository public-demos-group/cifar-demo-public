"""Console script for testproject."""

import click


@click.command()
def main() -> None:
    """Main."""
    click.echo("Scaffold for the CLI interface")


if __name__ == "__main__":
    main()  # pragma: no cover

"""Web application configuration for Uvicorn."""


class Settings:
    """Definition of application config. Used in OpenAPI /docs site."""

    PROJECT_NAME: str = "Showcase Project "
    APP_DESC: str = (
        "<h2>Try this app by uploading any image with `predict/image`</h2>"
        "<br>by Michał Chromiak"
    )
    PROJECT_VERSION: str = "1.0.0"


settings = Settings()

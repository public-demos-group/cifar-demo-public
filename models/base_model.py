import logging
import os
from abc import abstractmethod, ABC
from datetime import datetime

from models.model_configs.model_config import ModelConfig


class BaseModel(ABC):
    """Abstract Model class that is inherited to all models"""

    def __init__(self, m_cfg: ModelConfig):
        self.model_config = m_cfg
        self.dataset = None
        self.model = None

    @abstractmethod
    def _load_data(self):
        """Load dataset for the model"""
        raise NotImplementedError("Can't use '_load_data' on an ABC!")

    @abstractmethod
    def build(self):
        """Build model architecture"""
        raise NotImplementedError("Can't use 'build' on an ABC!")

    @abstractmethod
    def train(self):
        """Train model architecture"""
        raise NotImplementedError("Can't use 'train' on an ABC!")

    def evaluate(self):
        """evaluate model architecture"""
        raise NotImplementedError("Can't use 'evaluate' on an ABC!")

    @staticmethod
    def save_model(
            keras_model,
            path: str = '../output/models',
            file_name: str = 'model_file_name',
            save_format: str = 'tf',
    ) -> None:
        """Save model under path and filename of given format 'tf' or 'h5'.

        Args:
            keras_model: Model to be saved.
            path: Directory path to save model file.
            file_name: Name of model file.
            save_format: Model file save format tf or h5.

        Returns:
            None
        """
        if file_name == 'model_file_name':
            file_name = keras_model.__class__.__name__

        timestamp = datetime.now().strftime("%H_%M_%S")
        full_filename = ''.join([file_name, '-', timestamp, f".{save_format}"])

        os.makedirs(path, exist_ok=True)
        full_model_file_path = os.path.join(path, full_filename)
        logging.info(f"Saving model in: {full_model_file_path}")
        keras_model.save(full_model_file_path, save_format=save_format)
        logging.info(f"Model saved in: {full_model_file_path}")

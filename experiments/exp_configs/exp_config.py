"""Scaffold for model configuration."""
from dataclasses import dataclass

from models.model_configs.model_config import ModelConfig


@dataclass
class ExperimentConfig:
    """Store model training configuration options."""

    model_config: ModelConfig
    batch_size: int = 32
    number_epochs: int = 1
    optimizer: str = 'adam'
    loss: str = 'categorical_crossentropy'
    metrics: str = 'accuracy'

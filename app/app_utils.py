"""Module with helper functions that process requests data."""
import logging
from io import BytesIO

import numpy
import numpy as np
import tensorflow as tf
from PIL import Image

# from app.request_schemas import ImageClassificationRequest

logger = logging.getLogger('my_logger')

MODEL_PATH = (
    "/Users/mc/dev/DeepTale/cifar10-demo/output/models/"
    "CnnModelSubclassing-18_19_48.tf"
)
INPUT_SHAPE = (32, 32)


def read_image_file(file) -> Image.Image:
    """Read file  to PIL Image.

    Args:
        file: Uploaded file

    Returns:
        The PIL.Image file
    """
    image = Image.open(BytesIO(file))

    return image


def load_model() -> tf.keras.Model:
    """Load model from MODEL_PATH path.

    Returns:
        Loaded model for inference.
    """
    image_model = tf.keras.models.load_model(MODEL_PATH)

    return image_model


def predict(image: Image.Image, gpu: bool = True):  # , request:
    # ImageClassificationRequest):
    """Predict which class does the image is classified for. Uses CPU/GPU.

    Args:
        image: image for classification
        gpu: Bool flag defining if GPU (or CPU if False) should be used

    Returns:
        List of one-hot predictions for each classes from the model.
    """
    # Cifar10 class names
    _cifar10_class_names = {
        0: "Airplane",
        1: "Automobile",
        2: "Bird",
        3: "Cat",
        4: "Deer",
        5: "Dog",
        6: "Frog",
        7: "Horse",
        8: "Ship",
        9: "Truck",
    }
    model = load_model()

    img_tensor = np.asarray(image)
    resized_image = tf.image.resize(img_tensor, INPUT_SHAPE)
    img_tensor = np.array(img_tensor).astype(np.float32)
    img_tensor = numpy.expand_dims(img_tensor, axis=0)

    logger.info(f"Image resized to {INPUT_SHAPE}")

    device = tf.config.list_logical_devices('GPU')[0].name
    if not gpu:
        device = tf.config.list_logical_devices('CPU')[0].name
    tf.debugging.set_log_device_placement(True)

    with tf.device(device):
        result = model.predict(resized_image)
    logger.info(f"Prediction for device: {device}- result: {result}")

    resp = dict()
    for class_num, score in enumerate(*result):
        resp[str(_cifar10_class_names[class_num])] = f"{score * 100:0.2f} %"

    return resp

"""Store for model parameters"""
from dataclasses import dataclass
from typing import Tuple


@dataclass
class ModelConfig:
    input_shape: Tuple[int, int, int]
    output_dim: int

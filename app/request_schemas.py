"""Schemas for REST API calls."""

from pydantic import BaseModel, Field


class ImageClassificationRequest(BaseModel):
    """TODO: Request configuration for image classification."""

    simple_cnn: bool = Field(default=True, decription="Flag to use simple cnn")
    augmented: bool = Field(default=True, decription="Flag to use augmented "
                                                     "dataset")
    gpu: bool = Field(default=True, decription="Flag to use GPU; or CPU if "
                                               "False")

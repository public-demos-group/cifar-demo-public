"""Main module."""

import uvicorn
from config.app_config import settings
from fastapi import FastAPI, File, UploadFile, HTTPException
from starlette.responses import RedirectResponse

from app.app_utils import predict, read_image_file
from app.request_schemas import ImageClassificationRequest

app = FastAPI(
    title=settings.PROJECT_NAME,
    version=settings.PROJECT_VERSION,
    description=settings.APP_DESC,
)

default_request = ImageClassificationRequest()


@app.get("/", include_in_schema=False)
async def get_read_root():
    """Endpoint that redirects to OpenAPI site.

    Returns:
        Redirection to /docs
    """
    return RedirectResponse(url="/docs")


# Prediction endpoint with request validation
@app.post("/predict/image/")
async def post_prediction_for_image(
    image_file: UploadFile = File(...),
    gpu: bool = True
    # predict_request: ImageClassificationRequest = default_request,
):
    """POST endpoint that sends image file of jpg or png.

    It defines device with the gpu flag.

    Args:
        image_file: jpg or png file to be inferred on
        gpu: Bool flag defining usage of GPU (or CPU if False)

    Returns:
        Classification one-hot result for image class.
    """
    compatible_extension = image_file.filename.split(".")[-1] in (
        "jpg",
        "jpeg",
        "png",
    )
    if not compatible_extension:
        raise HTTPException(status_code=422, detail="[Unprocessable Entity] "
                                                    "File format not "
                                                    "supported")
    # Read file uploaded by the user
    image = read_image_file(await image_file.read())
    prediction = predict(image, gpu)  # , predict_request)

    return prediction


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)

"""Module responsible for handling REST requests."""


class ImageInferenceClassifier:
    """TODO: Class for more complex requests to configure choice of model."""

    def __init__(self, gpu: bool = True, model_name: str = "CNN"):
        """Store model's configuration options for REST call on inference.

        Args:
            gpu: string defining device to be used
            model_name: The name of the model to be used
        """
        self.model_name = model_name
        self.gpu = gpu

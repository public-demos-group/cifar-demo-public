"""Module responsible for training experiment of keras models."""

import logging
import os
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from keras.datasets import cifar10
from keras.optimizers import Adam
from keras.utils import to_categorical, plot_model
from keras_preprocessing.image import ImageDataGenerator
from sklearn.metrics import (
    confusion_matrix,
    classification_report,
)
from tensorflow import keras

from models.cnn_keras_models import (
    CnnModelSubclassing,
    SimpleCnnModelSubclassing,
)

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("../debug.log"), logging.StreamHandler()],
)
logger = logging.getLogger('my_logger')

BATCH_SIZE = 32
NUM_EPOCHS = 2

CLASS_NUM = 10


def plot_model_loss_acc(model_history):
    # """Plot training and validation loss and accuracy from history object."""

    # fig, ax = plt.subplots(figsize=(12, 4), nrows=1, ncols=2)
    # ax[0].plot(model_history.history["loss"], c="r", label="train loss")
    # ax[0].plot(model_history.history["val_loss"], c="b", label="val loss")
    # ax[0].set_xlabel("Epoch")
    # ax[0].set_ylabel("Loss")
    # ax[0].legend()
    #
    # ax[1].plot(model_history.history["accuracy"], c="r", label="train
    # accuracy")
    # ax[1].plot(
    #     model_history.history["val_accuracy"], c="b", label="val accuracy"
    # )
    # ax[1].set_xlabel("Epoch")
    # ax[1].set_ylabel("Accuracy")
    # ax[1].legend()
    #
    # fig.show()
    fig, loss_ax = plt.subplots()

    acc_ax = loss_ax.twinx()

    loss_ax.plot(model_history.history['loss'], 'y', label='train loss')
    loss_ax.plot(model_history.history['val_loss'], 'r', label='val loss')

    acc_ax.plot(model_history.history['accuracy'], 'b', label='train acc')
    acc_ax.plot(model_history.history['val_accuracy'], 'g', label='val acc')

    loss_ax.set_xlabel('epoch')
    loss_ax.set_ylabel('loss')
    acc_ax.set_ylabel('accuracy')

    loss_ax.legend(loc='upper left')
    acc_ax.legend(loc='lower left')

    plt.show()


def prepare_cifar10_dataset() -> tuple:
    """Download, scale and categorize labels for CIFAR10 dataset.

    Returns:
        Dataset split tuple.
    """
    logging.info("Loading CIFAR-10 dataset...")
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    logging.info("\t... CIFAR-10 downloaded.")

    logging.info(f"x_train shape: {x_train.shape}")
    logging.info(f"y_train shape: {y_train.shape}")
    logging.info(f"x_test shape: {x_test.shape}")
    logging.info(f"y_test shape: {y_test.shape}")

    # Cast int to float to be able to perform division by 255 (normalization)
    # Divide with 255.0 to normalize to [0,1] range
    x_train = x_train.astype('float32') / 255.0
    x_test = x_test.astype('float32') / 255.0

    num_class = CLASS_NUM
    # Convert class vectors to  encoding
    y_train = to_categorical(y_train, num_class)
    y_test = to_categorical(y_test, num_class)

    return (x_train, y_train), (x_test, y_test)


def train_and_evaluate(
    the_model_class: keras.Model, dataset: tuple, augment_data: bool = True
) -> tuple:
    """Train a train/test split 'dataset' with use of model class.

    Training can be enhanced with data augmentation decided with use of
    'augment_data' flag.

    Args:
        the_model_class: Model architecture to be compiled and trained.
        dataset: Train/test split dataset tuple with images and labels.
        augment_data: Flag controlling if the data should be augmented with
        ImageDataGenerator.

    Returns:
        Tuple of model architecture and the model training history object.
    """
    (x_train, y_train), (x_test, y_test) = dataset
    model_architecture = the_model_class(x_train.shape[1:], CLASS_NUM)

    optimizer = Adam(learning_rate=0.0001)

    # early stopping: monitor validation loss and avoid overfit
    early_stop = EarlyStopping(
        monitor='val_loss',
        mode='min',
        verbose=1,
        patience=10,
        restore_best_weights=True,
    )

    # reducing learning rate on plateau
    rlrop = ReduceLROnPlateau(
        monitor='val_loss',
        mode='min',
        patience=5,
        factor=0.5,
        min_lr=1e-6,
        verbose=1,
    )

    model_architecture.compile(
        optimizer=optimizer,
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )

    if augment_data:
        # construct the image generator for data augmentation
        data_gen = ImageDataGenerator(
            featurewise_center=True,
            featurewise_std_normalization=True,
            rotation_range=18,
            zoom_range=0.15,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.15,
            horizontal_flip=True,
            fill_mode="nearest",
        )
        # compute quantities required for featurewise normalization
        # (std, mean, and principal components if ZCA whitening is applied)
        data_gen.fit(x_train)

        train_generator = data_gen.flow(x_train, y_train, batch_size=BATCH_SIZE)
        test_generator = data_gen.flow(x_test, y_test, batch_size=BATCH_SIZE)

        # fits the model on batches with real-time data augmentation:
        logging.info("Training model with augmentations...")
        model_history = model_architecture.fit(
            x=train_generator,
            validation_data=test_generator,
            steps_per_epoch=x_train.shape[0] // BATCH_SIZE,
            epochs=NUM_EPOCHS,
            callbacks=[early_stop, rlrop],
            verbose=1,
            shuffle=True,
        )
        logging.info("... Finished training models with augmentations.")

    else:
        logging.info("Training model WITHOUT augmentations...")
        model_history = model_architecture.fit(
            x_train,
            y_train,
            batch_size=BATCH_SIZE,
            epochs=NUM_EPOCHS,
            validation_data=(x_test, y_test),
            shuffle=True,
        )
        logging.info("... Finished raining models WITHOUT augmentations.")

        evaluation = model_architecture.evaluate(x=x_test, y=y_test, verbose=1)
        logging.info(
            f"Test loss: {evaluation[0]} / Test accuracy: {evaluation[1]}"
        )

    # generate_model_stats(model_architecture, x_test, y_test)

    return model_architecture, model_history


def generate_model_stats(model_final: keras.Model, x_test, y_test):
    #
    # plot_model(
    #     model_final,
    #     to_file='../output/model_plot.png',
    #     show_shapes=True,
    #     show_layer_names=True,
    # )

    y_pred = model_final.predict(x_test)

    cm = confusion_matrix(np.argmax(y_test, axis=1), y_pred)
    print(cm)

    # report to see which category has been predicted incorectly and which
    # has been predicted correctly
    target = ["Category {}".format(i) for i in range(10)]
    print(
        classification_report(
            np.argmax(y_test, axis=1), y_pred, target_names=target
        )
    )

    ax = sns.heatmap(
        confusion_matrix(np.argmax(y_test, axis=1), np.argmax(y_pred, axis=1)),
        cmap="YlGnBu",
        annot=True,
        fmt="d",
    )
    plt.show(ax)


def save_model(
    keras_model,
    path: str = '../output/models',
    file_name: str = 'model_file_name',
    save_format: str = 'tf',
) -> None:
    """Save model under path and filename of given format 'tf' or 'h5'.

    Args:
        keras_model: Model to be saved.
        path: Directory path to save model file.
        file_name: Name of model file.
        save_format: Model file save format tf or h5.

    Returns:
        None
    """
    if file_name == 'model_file_name':
        file_name = keras_model.__class__.__name__

    timestamp = datetime.now().strftime("%H_%M_%S")
    full_filename = ''.join([file_name, '-', timestamp, f".{save_format}"])

    os.makedirs(path, exist_ok=True)
    full_model_file_path = os.path.join(path, full_filename)
    logging.info(f"Saving model in: {full_model_file_path}")
    keras_model.save(full_model_file_path, save_format=save_format)
    logging.info(f"Model saved in: {full_model_file_path}")


if __name__ == "__main__":
    data_set = prepare_cifar10_dataset()
    model_class_names = [SimpleCnnModelSubclassing, CnnModelSubclassing]
    for model_class_name in model_class_names:
        model, history = train_and_evaluate(model_class_name, data_set)
        save_model(model, file_name=model_class_name.__name__)
        plot_model_loss_acc(history)

        # tf.keras.utils.plot_model(model, show_shapes=True)

        # keras.utils.vis_utils.plot_model(
        #     model, f"../output/plots/{model_class}.png", show_shapes=True
        # )

PACKAGE_NAME = project-name
TAG = 0.1.0
PORT_DEV =   1410
PORT_PROD = 11410
CODE_PATH = $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
DATA_PATH = ${DATA_PATH_ENV}

sources = app

.PHONY: test format lint unittest train-cnn train-tl-efn pre-commit-install pre-commit
.PHONY: setup setup-prod

setup:
	poetry install
setup-prod:
	poetry install --without test,dev # run without poetry's test and dev group packages (see .toml file)

test: format lint unittest

format:
	poetry run isort $(sources) tests
	poetry run black $(sources) tests
lint:
	poetry run flake8 $(sources) tests
	poetry run mypy $(sources) tests
unittest:
	poetry run pytest

train-cnn:
	poetry run python experiments/cnn_keras_exp.py
train-tl-efn:
	poetry run python experiments/tl_efficient_net_b0_exp.py

pre-commit-install:
	poetry run pre-commit install
pre-commit:
	poetry run pre-commit run --all-files

build-local-dev:
	docker build --target development -t $(PACKAGE_NAME):$(TAG)-dev -f envs/Dockerfile .
build-local-prod:
	docker build --target production --build-arg PORT_PROD=$(PORT_PROD) -t $(PACKAGE_NAME):$(TAG)-prod -f envs/Dockerfile .
run-local-dev:
	docker run -it --rm -p=$(PORT_DEV):$(PORT_DEV) -v=$(CODE_PATH):/app -v=$(DATA_PATH):/opt/data/ $(PACKAGE_NAME):$(TAG)-dev bash
run-local-prod:
	docker run -it --rm -p=$(PORT_PROD):$(PORT_PROD) -v=$(DATA_PATH):/opt/data/ $(PACKAGE_NAME):$(TAG)-prod

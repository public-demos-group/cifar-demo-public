"""Module with simple cnn keras model based on Tensorflow subclassing
approach to building models.
"""

from keras.layers import (
    BatchNormalization,
    Conv2D,
    Dense,
    Dropout,
    Flatten,
    MaxPooling2D,
)
from tensorflow import keras


class SimpleCnnModelSubclassing(keras.Model):
    """Simple sequential model build with conv2d, maxpooling, flatten and
    dense layers.

    Model subclassing keras model.
    """

    def __init__(
        self,
        input_shape: tuple,
        classes_count: int = 10,
        model_name: str = 'model_name',
        **kwargs,
    ):
        if model_name == 'model_name':
            model_name = self.__class__.__name__
        super(SimpleCnnModelSubclassing, self).__init__(
            name=model_name, **kwargs
        )
        self.conv2D_1 = Conv2D(
            filters=32,
            kernel_size=(3, 3),
            input_shape=input_shape,  # (32, 32, 3) cifar10
            padding='same',
            activation="relu",
        )
        self.maxPooling2D_1 = MaxPooling2D(pool_size=(2, 2))
        self.conv2D_2 = Conv2D(64, (3, 3), padding='same', activation="relu")
        self.maxPooling2D_2 = MaxPooling2D(pool_size=(2, 2))
        self.flatten = Flatten()
        self.dense1 = Dense(512, activation="relu")
        self.dense2 = Dense(classes_count, activation="softmax")

    def call(self, inputs):
        x = self.conv2D_1(inputs)
        x = self.maxPooling2D_1(x)
        x = self.conv2D_2(x)
        x = self.maxPooling2D_2(x)
        x = self.flatten(x)
        x = self.dense1(x)
        x = self.dense2(x)

        return x


class CnnModelSubclassing(keras.Model):
    """Sequential model defined with subclassing.

    Based on Conv2D maxpooling, flatten and dense layers, but also improved
    with dropout and BatchNormalization layers.
    """

    def __init__(
        self,
        input_shape: tuple,
        classes_count: int = 10,
        model_name: str = 'model_name',
        **kwargs,
    ):
        if model_name == 'model_name':
            model_name = self.__class__.__name__
        super(CnnModelSubclassing, self).__init__(name=model_name, **kwargs)
        self.conv2D_1 = Conv2D(
            filters=32,
            kernel_size=(3, 3),
            input_shape=input_shape,  # (32, 32, 3) cifar10
            padding='same',
            activation="relu",
        )
        self.batchNormalization1 = BatchNormalization()
        self.maxPooling2D_1 = MaxPooling2D(pool_size=(2, 2))
        self.dropout1 = Dropout(0.25)
        self.conv2D_2 = Conv2D(64, (3, 3), padding='same', activation="relu")
        self.conv2D_3 = Conv2D(64, (3, 3), activation="relu")
        self.batchNormalization2 = BatchNormalization()
        self.maxPooling2D_2 = MaxPooling2D(pool_size=(2, 2))
        self.dropout2 = Dropout(0.25)
        self.flatten = Flatten()
        self.dense1 = Dense(512, activation="relu")
        self.dropout3 = Dropout(0.5)
        self.dense2 = Dense(classes_count, activation="softmax")

    def call(self, inputs):
        x = self.conv2D_1(inputs)
        x = self.batchNormalization1(x)
        x = self.maxPooling2D_1(x)
        x = self.dropout1(x)
        x = self.conv2D_2(x)
        x = self.conv2D_3(x)
        x = self.batchNormalization2(x)
        x = self.maxPooling2D_2(x)
        x = self.dropout2(x)
        x = self.flatten(x)
        x = self.dense1(x)
        x = self.dropout3(x)
        x = self.dense2(x)

        return x
